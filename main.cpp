#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <SOIL.h>

GLUquadric* pista;
GLUquadric* gameOver;
GLUquadric* roda;
GLUquadric* fire;
GLuint pista_tex;
GLuint fire_tex;
GLuint gameover_tex;


char ponto[5];

GLfloat eye[] = { 0, 0, 20 };
GLfloat at[] = { 0, 0, 0 };

float movimento = 0;

float movePista = 0;

bool gameOverBool = false;
float moveCarro = 0;
bool pause = true;

int pontos = 0;
float randX = 0;
float rivalSpeed = 0;
float moveYrival = 0;
int rivalSize = 1;

float roadSpeed = 0;
float randColorR = 0.4;
float randColorG = 0.6;
float randColorB = 0.3;


void imprime(float x, float y,float z, char *string)
{
    glRasterPos3f(x,y,z);

    int len = (int) strlen(string);

    for (int i = 0; i < len; i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,string[i]);
    }
}

static GLuint LoadItens(char* filename )
{
    GLuint tex;
    tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO,
                                SOIL_CREATE_NEW_ID,
                                SOIL_FLAG_INVERT_Y );
    if( 0 == tex )
    {
        printf( "erro do SOIL: '%s'\n", SOIL_last_result() );
    }

    // seta os parametros para a textura
    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_DECAL);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    return tex;
}




void draw(void)
{

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(eye[0], eye[1], eye[2], at[0], at[1], at[2], 0, 1, 0);

    if(eye[1]==0)
    {
        if(pause && !gameOverBool)
        {
            imprime(-6.5, 0, 5, "Aperte F1 para iniciar");
        }
        //contador
        imprime(12, 12, 5, ponto);
        imprime(-17, 12, 5, "F1 - Pause");
        imprime(-17, 10.5, 5, "F2 - Camera");
    }

    if(eye[1]== -18)
    {
        if(pause && !gameOverBool)
        {
            imprime(-6.5, 0, 6, "Aperte F1 para iniciar");
        }
        imprime(20, 10, 15, ponto);
        imprime(-30, 10.7, 14, "F1 - Pause");
        imprime(-30, 10, 12, "F2 - Camera");
    }

    if(eye[1]== -12)
    {
        if(pause && !gameOverBool)
        {
            imprime(-4.5+moveCarro, 0, 5, "Aperte F1 para iniciar");
        }
        imprime(20+moveCarro, 10, 12, ponto);
        imprime(-20+moveCarro, 10.7, 10, "F1 - Pause");
        imprime(-20+moveCarro, 10, 8, "F2 - Camera");
    }



    //imprime(-16, 9, 5, "F2 - MUDAR CAMERA");

    //carro
    glPushMatrix();
        glColor3f(0,0,1);
        glTranslatef(moveCarro,-13,2.3);
        glScaled(0.8,1.5,0.4);
        glutSolidCube(3);
    glPopMatrix();

    //farol traseiro
    glPushMatrix();
        glColor3f(1,0,0);
        glTranslatef(moveCarro,-15.2,2.6);
        glScaled(0.6,0.1,0.04);
        glutSolidCube(3);
    glPopMatrix();

    //farol dianteiro
    glPushMatrix();
        glColor3f(1,1,0);
        glTranslatef(moveCarro+0.7,-10.8,2.6);
        glScaled(0.3,0.2,0.1);
        glutSolidSphere(1, 32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(1,1,0);
        glTranslatef(moveCarro-0.7,-10.8,2.6);
        glScaled(0.3,0.2,0.1);
        glutSolidSphere(1, 32,16);
    glPopMatrix();


    // vidro
    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(moveCarro,-13.3,3.0);
        glScaled(0.8,1.2,0.4);
        glutSolidCube(2);
    glPopMatrix();

    // parte de cima
    glPushMatrix();
        glColor3f(0,0,1);
        glTranslatef(moveCarro,-13.3,3.45);
        glScaled(0.8,1.2,0.05);
        glutSolidCube(2);
    glPopMatrix();

    //rodas

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(moveCarro+1.3,-14.3,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(moveCarro+1.3,-11.5,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(moveCarro-1.3,-14.3,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(moveCarro-1.3,-11.5,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();


    glColor3f(1,1,1);



    //pista
    glPushMatrix();
        glTranslatef(-10, movePista+23,-41);
        glRotatef(90, 0, 1, 0);
        glRotatef(45, 0,0, 1);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, pista_tex);
        gluQuadricDrawStyle(pista, GLU_FILL);
        glBindTexture(GL_TEXTURE_2D, pista_tex);
        gluQuadricTexture(pista, GL_TRUE);
        gluQuadricNormals(pista, GLU_SMOOTH);
        gluCylinder(pista, 60, 60, 20, 4, 1);
        glDisable(GL_TEXTURE_2D);
    glPopMatrix();


    // carro inimigo
    glPushMatrix();
        glTranslatef(randX, moveYrival, 2.3);
        glColor3f(randColorR,randColorG,randColorB);
        glScaled(0.8,1.5,0.4);
        glutSolidCube(3);
    glPopMatrix();

    //farol traseiro
    glPushMatrix();
        glColor3f(1,0,0);
        glTranslatef(randX,-13+moveYrival+15.2,2.6);
        glScaled(0.6,0.1,0.04);
        glutSolidCube(3);
    glPopMatrix();

    //farol dianteiro
    glPushMatrix();
        glColor3f(1,1,0);
        glTranslatef(randX+0.7,-13+moveYrival+10.7,2.6);
        glScaled(0.3,0.2,0.1);
        glutSolidSphere(1, 32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(1,1,0);
        glTranslatef(randX-0.7,-13+moveYrival+10.7,2.6);
        glScaled(0.3,0.2,0.1);
        glutSolidSphere(1, 32,16);
    glPopMatrix();


    // vidro
    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(randX,-13+moveYrival+13.3,3.0);
        glScaled(0.8,1.2,0.4);
        glutSolidCube(2);
    glPopMatrix();

    // parte de cima
    glPushMatrix();
        glColor3f(randColorR,randColorG,randColorB);
        glTranslatef(randX,-13+moveYrival+13.3,3.45);
        glScaled(0.8,1.2,0.05);
        glutSolidCube(2);
    glPopMatrix();

    //rodas

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(randX+1.3,-13+moveYrival+14.3,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(randX+1.3,-13+moveYrival+11.5,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(randX-1.3,-13+moveYrival+14.3,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glPushMatrix();
        glColor3f(0,0,0);
        glTranslatef(randX-1.3,-13+moveYrival+11.5,1.9);
        glRotatef(90,0,1,0);
        glScaled(0.1,0.1,0.1);
        glutSolidTorus(1,4,32,16);
    glPopMatrix();

    glColor3f(1,1,1);

    if (gameOverBool)
    {
        // explosao
        glPushMatrix();
            glTranslatef(moveCarro, -12, 1.7);
            glRotatef(-movimento*10, 1, 0, 0);
            glRotatef(-movimento*2, 0, 1, 0);
            glRotatef(-movimento*7, 0, 0, 1);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, fire_tex);
            gluQuadricDrawStyle(fire, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, fire_tex);
            gluQuadricTexture(fire, GL_TRUE);
            gluQuadricNormals(fire, GLU_SMOOTH);
            gluSphere(fire, 3, 32, 16);
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();


        //gameover
        glPushMatrix();
            if(eye[1] == 0 )
            {
                glTranslatef(0, 0,15);
                glRotatef(270, 1,0, 0);
            }
            else
            {
                glTranslatef(0, -8,7);
            }

            glRotatef(-movimento, 0,0, 1);
            glScaled(1, 1, 1);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, gameover_tex);
            gluQuadricDrawStyle(gameOver, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, gameover_tex);
            gluQuadricTexture(gameOver, GL_TRUE);
            gluQuadricNormals(gameOver, GLU_SMOOTH);
            gluCylinder(gameOver, 2,2, 2, 20, 1);
            glDisable(GL_TEXTURE_2D);

        glPopMatrix();

    }


    glutSwapBuffers();
}

void rotation(int value)
{
    //mudar camera na batida em primeira pessoa

    if (gameOverBool && eye[1] == -12)
    {
        eye[0] = 0;
        eye[1] = -18;
        eye[2] = 6;
        at[0] = 0;
    }

    if(!pause)
    {


        if ((!gameOverBool))
        {

            movePista-= 1;
            rivalSpeed-= (1+roadSpeed);
            moveYrival = rivalSpeed + 20;

            //pista infinita
            if (movePista<=-43)
            {
                movePista=0;
            }

            //cor dos carros e contador
            if (rivalSpeed<=-43)
            {
                pontos++;
                sprintf(ponto, "%d", pontos);

                //printf("Pontos: %d\n", pontos);

                int r = rand()%100;// 0.00 to 0.99
                randColorR = r/100.0;

                int g = rand()%100;// 0.00 to 0.99
                randColorG = g/100.0;

                int b = rand()%100;// 0.00 to 0.99
                randColorB = b/100.0;
            }

            //aumenta a velocidade dos rivais
            if (rivalSpeed<=-43)
            {
                rivalSpeed = 15;
                randX = rand() % 18 + (-8);
                if (roadSpeed < 3)
                {
                    roadSpeed+=0.05;
                }
            }

            //colisao
            if (moveYrival <= -11 && moveYrival >= -15)
            {
                if (!((randX >= moveCarro+2.5) || (randX <= moveCarro-2.5)))
                {
                    gameOverBool = true;
                }
            }
        }
    }

    movimento += 5;
    glutPostRedisplay();
    glutTimerFunc(50, rotation, 1);

}

void resize(int w, int h)
{
    if (!h)
        h = 1;
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1.0*w/h, 0.1, 100.0);
}

void init(void)
{

    /* Initialize the camera position */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //gluLookAt(eye[0], eye[1], eye[2], at[0], at[1], at[2], 0, 1, 0);


    glEnable(GL_DEPTH_TEST);
    pista = gluNewQuadric();
    roda = gluNewQuadric();
    fire = gluNewQuadric();
    gameOver= gluNewQuadric();

    glEnable(GL_TEXTURE_2D);

    pista_tex = LoadItens("C:\\street.jpg");
    gameover_tex = LoadItens("C:\\gameOver.png");
    fire_tex = LoadItens("C:\\fire.jpg");
}



void SpecialKey(int key, int x, int y)
{
    switch (key)
    {

    case GLUT_KEY_LEFT:
        if(moveCarro>=-7.0 && !gameOverBool && !pause)
            moveCarro--;
        {
            if(eye[1]==-12)
            {
                eye[0] = moveCarro;
                at[0] = moveCarro;
            }
        }
        break;
    case GLUT_KEY_RIGHT:
        if(moveCarro<=7.0 && !gameOverBool && !pause)
        {
            moveCarro++;

            if(eye[1]==-12)
            {
                eye[0] = moveCarro;
                at[0] = moveCarro;

            }
        }


        break;

    case GLUT_KEY_F2:
        //terceira pessoa
        if(eye[1] == 0)
        {
            eye[0] = 0;
            eye[1] = -18;
            eye[2] = 6;
            at[0] = 0;
        }
        //primeira pessoa
        else if(eye[1]== -18 && gameOverBool)
        {
            eye[0] = 0;
            eye[1] = 0;
            eye[2] = 20;
            at[0] = 0;
        }
        else if(eye[1]== -18)
        {
            eye[0] = moveCarro;
            at[0] = moveCarro;
            eye[1] = -12;
            eye[2] = 4;
        }
        //superior
        else if(eye[1]== -12)
        {
            eye[0] = 0;
            eye[1] = 0;
            eye[2] = 20;
            at[0] = 0;
        }

        break;
    case GLUT_KEY_F1:
        if(!pause)
        {
            pause = true;
        }
        else
        {
            pause = false;
        }
        break;
    }

    glutPostRedisplay();
}




int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(640, 512);

    glutCreateWindow("Corrida");

    glutDisplayFunc(draw);
    glutReshapeFunc(resize);
    glutTimerFunc(50, rotation, 1);

    glutSpecialFunc(SpecialKey);



    init();

    glutMainLoop();
}
